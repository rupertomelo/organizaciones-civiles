const UsuarioDB = require("../db/User");
const OrganizacionDB = require("../db/Organizacion");
const ProyectoDB = require("../db/Proyecto");
const EquipamientoDB = require("../db/Equipamiento");
const PremioDB = require("../db/Premio");
const nodemailer = require('nodemailer');
const fs = require("fs");
const jwt = require('jwt-simple');
const secret = "0r64n1Z4c1oN3s";

const AccountMail = nodemailer.createTransport({
  host: 'smtp.gmail.com',
  port: 465,
  secure: true,
  auth: {
    user: 'apps.innovacion@hidalgo.gob.mx',
    pass: 'innovacion2019'
  }
});

module.exports = (app) => {
	app.post("/Alta", (request, response) => {
		try{
			const alta = new UsuarioDB(request.body);
				var data = request.body;
				alta.save((err) => {
					if(err){
						return response.send({ error: err });
					}else{
						let mailOptions = {
					        from: "Organizaciones de la Sociedad Civil del Estado de Hidalgo",
					        to: data.correoorg,
					        subject: "¡Organización Registrada Exitosamente!",
					        text: "¡Organización Registrada Exitosamente!**",
					        cc:'ruperto107@gmail.com',
					        html: '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">'+
					              '<html xmlns="http://www.w3.org/1999/xhtml">'+
					              '<head>'+
					              '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />'+
					              '<meta name="viewport" content="width=device-width, initial-scale=1" />'+
					              '<title>Oxygen Invoice</title>'+
					              '<style type="text/css">'+
					                '* {'+
					                  'font-family: Helvetica, Arial, sans-serif;'+
					                '}    '+
					                'td {'+
					                  'font-family: Helvetica, Arial, sans-serif;'+
					                  'font-size: 14px;'+
					                  'color: #777777;'+
					                  'text-align: center;'+
					                  'line-height: 21px;'+
					                '}    '+
					                '.header-lg,'+
					                '.header-md,'+
					                '.header-sm {'+
					                  'font-size: 25px;'+
					                  'font-weight: 700;'+
					                  'line-height: normal;'+
					                  'padding: 35px 0 0;'+
					                  'color: #4d4d4d;'+
					                '}    '+
					                '.header-md {'+
					                  'font-size: 24px;'+
					                '}    '+
					                '.header-sm {'+
					                  'padding: 5px 0;'+
					                  'font-size: 18px;'+
					                  'line-height: 1.3;'+
					                '}    '+
					                '.content-padding {'+
					                  'padding: 20px 0 5px;'+
					                '}'+
					                '.mobile-header-padding-right {'+
					                  'width: 290px;'+
					                  'text-align: right;'+
					                  'padding-left: 10px;'+
					                '}'+
					                '.mobile-header-padding-left {'+
					                  'width: 290px;'+
					                  'text-align: left;'+
					                  'padding-left: 10px;'+
					                '}'+
					                '.free-text {'+
					                  'width: 100% !important;'+
					                  'padding: 10px 60px 0px;'+
					                '}'+
					                '.button {'+
					                  'padding: 30px 0;'+
					                '}  '+
					              '</style>'+
					              '<style type="text/css" media="screen">'+
					                '@import url(http://fonts.googleapis.com/css?family=Oxygen:400,700);'+
					              '</style>'+
					              '<style type="text/css" media="screen">'+
					                '@media screen {'+
					                  '* {'+
					                      'font-family: "Oxygen", "Helvetica Neue", "Arial", "sans-serif";'+
					                    '}   '+
					                 '}  '+
					              '</style>'+
					              '<style type="text/css" media="only screen and (max-width: 480px)">'+
					                '/* Mobile styles */'+
					                '@media only screen and (max-width: 480px) {'+
					                  'table[class*="container-for-gmail-android"] {'+
					                    'min-width: 290px !important;'+
					                    'width: 100% !important;'+
					                  '}'+
					                  'img[class="force-width-gmail"] {'+
					                    'display: none !important;'+
					                    'width: 0 !important;'+
					                    'height: 0 !important;'+
					                  '}'+
					                  'table[class="w320"] {'+
					                    'width: 320px !important;'+
					                  '}'+
					                  'td[class*="mobile-header-padding-left"] {'+
					                    'width: 160px !important;'+
					                    'padding-left: 0 !important;'+
					                  '}'+
					                  'td[class*="mobile-header-padding-right"] {'+
					                    'width: 160px !important;'+
					                    'padding-right: 0 !important;'+
					                  '}'+
					                  'td[class="header-lg"] {'+
					                    'font-size: 24px !important;'+
					                    'padding-bottom: 5px !important;'+
					                  '}'+
					                  'td[class="content-padding"] {'+
					                    'padding: 5px 0 5px !important;'+
					                  '}'+
					                   'td[class="button"] {'+
					                    'padding: 5px 5px 30px !important;'+
					                  '}'+
					                  'td[class*="free-text"] {'+
					                    'padding: 10px 18px 30px !important;'+
					                  '}'+
					                  'td[class~="mobile-hide-img"] {'+
					                    'display: none !important;'+
					                    'height: 0 !important;'+
					                    'width: 0 !important;'+
					                    'line-height: 0 !important;'+
					                  '}'+
					                  'td[class~="item"] {'+
					                    'width: 140px !important;'+
					                    'vertical-align: top !important;'+
					                  '}'+
					                  'td[class~="quantity"] {'+
					                    'width: 50px !important;'+
					                  '}'+
					                  'td[class~="price"] {'+
					                    'width: 90px !important;'+
					                  '}'+
					                  'td[class="item-table"] {'+
					                    'padding: 30px 20px !important;'+
					                  '}'+
					                  'td[class="mini-container-left"],'+
					                  'td[class="mini-container-right"] {'+
					                    'padding: 0 15px 15px !important;'+
					                    'display: block !important;'+
					                    'width: 290px !important;'+
					                  '}'+
					                '}'+
					              '</style>'+
					            '</head>'+
					            '<body bgcolor="#f7f7f7">'+
					            '<table align="center" cellpadding="0" cellspacing="0" class="container-for-gmail-android" width="100%">'+
					              '<tr>'+
					                '<td align="left" valign="top" width="100%" style="background:repeat-x url(http://s3.amazonaws.com/swu-filepicker/4E687TRe69Ld95IDWyEg_bg_top_02.jpg) #ffffff;">'+
					                  '<center>'+
					                  '<img src="http://s3.amazonaws.com/swu-filepicker/SBb2fQPrQ5ezxmqUTgCr_transparent.png" class="force-width-gmail">'+
					                    '<table cellspacing="0" cellpadding="0" width="100%" bgcolor="#ffffff" background="http://s3.amazonaws.com/+swu-filepicker/4E687TRe69Ld95IDWyEg_bg_top_02.jpg" style="background-color:transparent">'+
					                      '<tr>'+
					                        '<td width="100%" height="80" valign="top" style="text-align: center; vertical-align:middle;">'+
					                          '<center style="background: #112835">'+
					                            '<table cellpadding="0" cellspacing="0" width="600" class="w320">'+
					                              '<tr>'+
					                                '<td class="pull-left mobile-header-padding-left" style="vertical-align: middle;">'+
					                                  // '<a href=""><img width="137" height="47" src="http://cdn.hidalgo.gob.mx/logo_name.png" alt="logo"></a>'+
					                                '</td>'+
					                              '</tr>'+
					                            '</table>'+
					                          '</center>'+
					                        '</td>'+
					                      '</tr>'+
					                    '</table>'+
					                  '</center>'+
					                '</td>'+
					              '</tr>'+
					              '<tr>'+
					                '<td align="center" valign="top" width="100%" style="background-color: #f7f7f7;" class="content-padding">'+
					                  '<center>'+
					                    '<table cellspacing="0" cellpadding="0" width="600" class="w320">'+
					                      '<tr>'+
					                        '<td class="header-lg">'+
					                          '¡Organización Registrada Exitosamente!'+
					                        '</td>'+
					                      '</tr>'+
					                      '<tr>'+
					                        '<td class="free-text">'+
					                          '<p>Le informamos que su organización a quedado registrada correctamente '+
					                          'con la siguiente información que utilizará para acceder a la plataforma: <br/>'+
					                          '<div style="text-align:left">'+
					                          	'<b>RFC de la Organización: </b>'+ data.rfcorg + '<br/>'+
					                          	'<b>Correo de la Organización: </b>'+ data.correoorg + '<br/>'+
					                          	'<b>Contraseña de la Organización: </b>'+ data.passorg + '<br/><br/>'+
					                           '</div>'+
					                          '<span>Ingrese a la plataforma para continuar con su registro.'+
					                        '</td>'+
					                      '</tr>'+
					                      '<tr>'+
					                        '<td'+
					                          '<div><a href="http://localhost:8075/login"'+
					                          '><button style="background: green;color: white; padding: 10px; border-radius: 5px;border: none;'+
    											'letter-spacing: 2px;">Ingresar a la plataforma</button></a></div>'+
					                        '</td>'+
					                      '</tr>'+
					                    '</table>'+
					                  '</center>'+
					                '</td>'+
					              '</tr>'+
					            '</table>'+
					            '</div>'+
					            '</body>'+
					            '</html>'
					      };

					      AccountMail.sendMail(mailOptions, (error, info) => {
					        if (error){
					          return console.log(error);
					        }
					      });
					return response.send({ success: "OK" });
					}
				});
			}catch(e){
			return response.send({ error: "TRY CATCH" });
		}
	});

	app.post("/Login", (request, response) => {
		try{
			UsuarioDB.findOne(request.body, (err, document) => {
				if(!err && document){
					const session = jwt.encode(document, secret);
					response.cookie('auth', session, { maxAge: 900000 * 60, httpOnly: true });
					return response.send({ success: "OK" });
				}else{
					return response.send({ error: "DONT USER" });
				}
			});
		}catch(e){
			return response.send({ error: "TRY CATCH" });
		}
	});

	app.post("/nueva/organizacion", (request, response) => {
		// console.log(request.body);
		try{
				const nuevaOrganizacion = new OrganizacionDB(request.body);
				nuevaOrganizacion.save((err) => {
					if(err){
						return response.send({ error: err });
					}else{
						return response.send({ success: "OK" });
					}
				});
			}catch(e){
			return response.send({ error: "TRY CATCH" });
		}
	});

	app.post("/nuevo/proyecto", (request, response) => {
		try{
			// console.log(request.body);
			request.body['fecharegistro'] = Number(new Date());
				const nuevoProyecto = new ProyectoDB(request.body);
				nuevoProyecto.save((err) => {
					if(err){
						return response.send({ error: err });
					}else{
						return response.send({ success: "OK" });
					}
				});
			}catch(e){
			return response.send({ error: "TRY CATCH" });
		}
	});

	app.post("/nuevo/equipamiento", (request, response) => {
		request.body['fecharegistro'] = Number(new Date());
		try{
				const nuevoEquipamiento = new EquipamientoDB(request.body);
				nuevoEquipamiento.save((err) => {
					if(err){
						return response.send({ error: err });
					}else{
						return response.send({ success: "OK" });
					}
				});
			}catch(e){
			return response.send({ error: "TRY CATCH" });
		}
	});

	app.post("/nuevo/premio", (request, response) => {
		request.body['fecharegistro'] = Number(new Date());
		try{
				const nuevoPremio = new PremioDB(request.body);
				nuevoPremio.save((err) => {
					if(err){
						return response.send({ error: err });
					}else{
						return response.send({ success: "OK" });
					}
				});
			}catch(e){
			return response.send({ error: "TRY CATCH" });
		}
	});

	app.post('/validar/:_id', function(request, response) {
		var fecha = new Date(Number(request.body.fechavalidacion));
		var fechaformat = fecha.getDate()+"/"+(fecha.getMonth()+1)+"/"+fecha.getFullYear();
		try{
			OrganizacionDB.findOneAndUpdate({
				_id: request.params._id
			}, {
				$set: request.body
			}, (err) => {
				if(err){
					return response.send({ error: err });
				}else{
					let mailOptions = {
					        from: "Organizaciones de la Sociedad Civil del Estado de Hidalgo",
					        to: request.body.correo,
					        subject: "¡Validación de su organización!",
					        text: "¡Organización Validada Exitosamente!**",
					        // cc:'ruperto107@gmail.com',
					        html: '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">'+
					              '<html xmlns="http://www.w3.org/1999/xhtml">'+
					              '<head>'+
					              '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />'+
					              '<meta name="viewport" content="width=device-width, initial-scale=1" />'+
					              '<title>Oxygen Invoice</title>'+
					              '<style type="text/css">'+
					                '* {'+
					                  'font-family: Helvetica, Arial, sans-serif;'+
					                '}    '+
					                'td {'+
					                  'font-family: Helvetica, Arial, sans-serif;'+
					                  'font-size: 14px;'+
					                  'color: #777777;'+
					                  'text-align: center;'+
					                  'line-height: 21px;'+
					                '}    '+
					                '.header-lg,'+
					                '.header-md,'+
					                '.header-sm {'+
					                  'font-size: 25px;'+
					                  'font-weight: 700;'+
					                  'line-height: normal;'+
					                  'padding: 35px 0 0;'+
					                  'color: #4d4d4d;'+
					                '}    '+
					                '.header-md {'+
					                  'font-size: 24px;'+
					                '}    '+
					                '.header-sm {'+
					                  'padding: 5px 0;'+
					                  'font-size: 18px;'+
					                  'line-height: 1.3;'+
					                '}    '+
					                '.content-padding {'+
					                  'padding: 20px 0 5px;'+
					                '}'+
					                '.mobile-header-padding-right {'+
					                  'width: 290px;'+
					                  'text-align: right;'+
					                  'padding-left: 10px;'+
					                '}'+
					                '.mobile-header-padding-left {'+
					                  'width: 290px;'+
					                  'text-align: left;'+
					                  'padding-left: 10px;'+
					                '}'+
					                '.free-text {'+
					                  'width: 100% !important;'+
					                  'padding: 10px 60px 0px;'+
					                '}'+
					                '.button {'+
					                  'padding: 30px 0;'+
					                '}  '+
					              '</style>'+
					              '<style type="text/css" media="screen">'+
					                '@import url(http://fonts.googleapis.com/css?family=Oxygen:400,700);'+
					              '</style>'+
					              '<style type="text/css" media="screen">'+
					                '@media screen {'+
					                  '* {'+
					                      'font-family: "Oxygen", "Helvetica Neue", "Arial", "sans-serif";'+
					                    '}   '+
					                 '}  '+
					              '</style>'+
					              '<style type="text/css" media="only screen and (max-width: 480px)">'+
					                '/* Mobile styles */'+
					                '@media only screen and (max-width: 480px) {'+
					                  'table[class*="container-for-gmail-android"] {'+
					                    'min-width: 290px !important;'+
					                    'width: 100% !important;'+
					                  '}'+
					                  'img[class="force-width-gmail"] {'+
					                    'display: none !important;'+
					                    'width: 0 !important;'+
					                    'height: 0 !important;'+
					                  '}'+
					                  'table[class="w320"] {'+
					                    'width: 320px !important;'+
					                  '}'+
					                  'td[class*="mobile-header-padding-left"] {'+
					                    'width: 160px !important;'+
					                    'padding-left: 0 !important;'+
					                  '}'+
					                  'td[class*="mobile-header-padding-right"] {'+
					                    'width: 160px !important;'+
					                    'padding-right: 0 !important;'+
					                  '}'+
					                  'td[class="header-lg"] {'+
					                    'font-size: 24px !important;'+
					                    'padding-bottom: 5px !important;'+
					                  '}'+
					                  'td[class="content-padding"] {'+
					                    'padding: 5px 0 5px !important;'+
					                  '}'+
					                   'td[class="button"] {'+
					                    'padding: 5px 5px 30px !important;'+
					                  '}'+
					                  'td[class*="free-text"] {'+
					                    'padding: 10px 18px 30px !important;'+
					                  '}'+
					                  'td[class~="mobile-hide-img"] {'+
					                    'display: none !important;'+
					                    'height: 0 !important;'+
					                    'width: 0 !important;'+
					                    'line-height: 0 !important;'+
					                  '}'+
					                  'td[class~="item"] {'+
					                    'width: 140px !important;'+
					                    'vertical-align: top !important;'+
					                  '}'+
					                  'td[class~="quantity"] {'+
					                    'width: 50px !important;'+
					                  '}'+
					                  'td[class~="price"] {'+
					                    'width: 90px !important;'+
					                  '}'+
					                  'td[class="item-table"] {'+
					                    'padding: 30px 20px !important;'+
					                  '}'+
					                  'td[class="mini-container-left"],'+
					                  'td[class="mini-container-right"] {'+
					                    'padding: 0 15px 15px !important;'+
					                    'display: block !important;'+
					                    'width: 290px !important;'+
					                  '}'+
					                '}'+
					              '</style>'+
					            '</head>'+
					            '<body bgcolor="#f7f7f7">'+
					            '<table align="center" cellpadding="0" cellspacing="0" class="container-for-gmail-android" width="100%">'+
					              '<tr>'+
					                '<td align="left" valign="top" width="100%" style="background:repeat-x url(http://s3.amazonaws.com/swu-filepicker/4E687TRe69Ld95IDWyEg_bg_top_02.jpg) #ffffff;">'+
					                  '<center>'+
					                  '<img src="http://s3.amazonaws.com/swu-filepicker/SBb2fQPrQ5ezxmqUTgCr_transparent.png" class="force-width-gmail">'+
					                    '<table cellspacing="0" cellpadding="0" width="100%" bgcolor="#ffffff" background="http://s3.amazonaws.com/+swu-filepicker/4E687TRe69Ld95IDWyEg_bg_top_02.jpg" style="background-color:transparent">'+
					                      '<tr>'+
					                        '<td width="100%" height="80" valign="top" style="text-align: center; vertical-align:middle;">'+
					                          '<center style="background: #112835">'+
					                            '<table cellpadding="0" cellspacing="0" width="600" class="w320">'+
					                              '<tr>'+
					                                '<td class="pull-left mobile-header-padding-left" style="vertical-align: middle;">'+
					                                  // '<a href=""><img width="137" height="47" src="http://cdn.hidalgo.gob.mx/logo_name.png" alt="logo"></a>'+
					                                '</td>'+
					                              '</tr>'+
					                            '</table>'+
					                          '</center>'+
					                        '</td>'+
					                      '</tr>'+
					                    '</table>'+
					                  '</center>'+
					                '</td>'+
					              '</tr>'+
					              '<tr>'+
					                '<td align="center" valign="top" width="100%" style="background-color: #f7f7f7;" class="content-padding">'+
					                  '<center>'+
					                    '<table cellspacing="0" cellpadding="0" width="600" class="w320">'+
					                      '<tr>'+
					                        '<td class="header-lg">'+
					                          '¡Organización Validada Exitosamente!'+
					                        '</td>'+
					                      '</tr>'+
					                      '<tr>'+
					                        '<td class="free-text">'+
					                          '<p>Le informamos que su organización a quedado validada con la siguiente información: <br/>'+
					                          '<div style="text-align:left">'+
					                          	'<b>Estatus de la organización: </b>'+ request.body.estatus + '<br/>'+
					                          	'<b>Observaciones de la validación: </b>'+ request.body.observaciones + '<br/>'+
					                          	'<b>Fecha de validación: </b>'+ fechaformat + '<br/>'+'<br/><br/>'+
					                           '</div>'+
					                          '<span>Ingrese a la plataforma para continuar la demás acciones'+
					                        '</td>'+
					                      '</tr>'+
					                      '<tr>'+
					                        '<td'+
					                          '<div><a href="http://localhost:8075/Login"'+
					                          '><button style="background: green;color: white; padding: 10px; border-radius: 5px;border: none;'+
    											'letter-spacing: 2px;">Ingresar a la plataforma</button></a></div>'+
					                        '</td>'+
					                      '</tr>'+
					                    '</table>'+
					                  '</center>'+
					                '</td>'+
					              '</tr>'+
					            '</table>'+
					            '</div>'+
					            '</body>'+
					            '</html>'
					      };

					      AccountMail.sendMail(mailOptions, (error, info) => {
					        if (error){
					          return console.log(error);
					        }
					      });
					return response.send({ success: "OK" });
				}
			});
		}catch(e){
			return response.send({ error: "TRY CATCH" });
		}
	});

	app.post('/update/organizacion/:_id', function(request, response) {
		// console.log("llego",request.body);
		try{
			OrganizacionDB.findOneAndUpdate({
				_id: request.params._id
			}, {
				$set: request.body
			}, (err) => {
				if(err){
					return response.send({ error: err });
				}else{
					return response.send({ success: "OK" });
				}
			});
		}catch(e){
			return response.send({ error: "TRY CATCH" });
		}
	});



	app.post("/subir/archivo/:_id/:extension/:type", (request, response) => {
		try{
			const _id = request.params._id;
			const extension = request.params.extension;
			const name = Number(new Date()) + _id +"."+ extension;
			request.files.archivo.mv("./temp_files/"+name, () => {
				const fs_write_stream = fs.createReadStream("./temp_files/"+name);
				const readstream = app.grid_file_system.createWriteStream({
					filename: name,
					content_type: request.params.type.split("_").join("/")
				});
				fs_write_stream.pipe(readstream);
				readstream.on('close', function () {
					fs.unlinkSync("./temp_files/"+name)
					return response.send({ success: name });
				});
			});
		}catch(e){
			return response.send({ error: "TRY CATCH" });
		}
	});

	app.get("/show/file/:id", (request, response) => {
		try{
			app.grid_file_system.files.findOne({ filename: request.params.id }, function (err, file) {
				if (err) {
					console.log(err);
					throw (err);
				}
				response.setHeader('Content-Type', file.contentType);
				var fs_write_stream = fs.createWriteStream('write.txt');
				var readstream = app.grid_file_system.createReadStream({
					filename: request.params.id
				});
				readstream.pipe(response);
			});

		}catch(e){
			console.log(e);
			return response.send({ error: "TRY CATCH" });
		}
	});


};