const mongoose = require("mongoose");

var Fecha = Number;
var File = String;
var Latitud = Number;
var Longitud = Number;
var GeoPointType = [Longitud, Latitud];

var GeoPoint = new mongoose.Schema({
	type: { type: String, default: "Point" },
    coordinates: { type: GeoPointType }
});

var Actividades = new mongoose.Schema({
	actividad1: { type: Boolean },
	actividad2: { type: Boolean },
	actividad3: { type: Boolean },
	actividad4: { type: Boolean },
	actividad5: { type: Boolean },
	actividad6: { type: Boolean },
	actividad7: { type: Boolean },
	actividad8: { type: Boolean },
	actividad9: { type: Boolean },
	actividad10: { type: Boolean },
	actividad11: { type: Boolean },
	actividad12: { type: Boolean },
	actividad13: { type: Boolean },
	actividad14: { type: Boolean },
	actividad15: { type: Boolean },
	actividad16: { type: Boolean },
	actividad17: { type: Boolean },
	actividad18: { type: Boolean }
});

var Programa = new mongoose.Schema({
	programanombre: { type: String },
	programadescripcion: { type: String },
	programabeneficiarios: { type: String}
});

var Adicional = new mongoose.Schema({
	adicionaltipo: { type: String },
	adicionaldescripcion: { type: String },
	adicionalfechaemision: { type: Fecha },
	adicionalcomprobante:{ type: String }
});

var Organizacion = new mongoose.Schema({
	rfc: {type: String},
	nombre: { type: String },
	descripcion:{ type:String },
	cluni: { type: String },
	estatus: { type:String },
	logotipo: { type: File },
	palabras_claves:  { type: [String] },
	foto_1: { type: File },
	foto_2: { type: File },
	foto_3: { type: File },
	foto_4: { type: File },
	coordenadas: { type: GeoPoint },
	no_cluni:{ type: String },
	rfc: { type: String },
	municipio: { type: String },
	temas: { type: [String] },
	donataria: { type: String },
	fecha: { type: Fecha },
	telefono: { type: String },
	correo: { type: String },
	pagina: { type: String },
	facebook: { type: String },
	twitter: { type: String },
	otra_red: { type: String },
	no_personas: { type: Number },
	no_voluntarios: { type: Number },
	mision: { type: String },
	vision: { type: String },
	junta_gral: { type: String },
	objeto: { type: String },
	actividades: { type: [Boolean] }, 
	domfiscalcalle: { type: String },
	domfiscalnoext: { type: String },
	domfiscalnoint: { type: String },
	domfiscalcolonia: { type: String },
	domfiscalcp: { type: String },
	domfiscalmunicipio: { type: String },
	domsocial: { type: String },
	domsocialcalle: { type: String },
	domsocialnoext: { type: String },
	domsocialnoint: { type: String },
	domsocialcolonia: { type: String },
	domsocialcp: { type: String },
	domsocialmunicipio: { type: String },
	rlnombre: { type: String },
	rlapaterno: { type: String },
	rlamaterno: { type: String },
	rlfechanac: { type: Fecha },
	rlcurp: { type: String },
	rlrfc: { type: String },
	rltelefono: { type: String },
	rlcorreo: { type: String },
	rldomicilio: { type: String },
	rlvalida: { type: String },
	rldg: { type: String },
	rldgcurp: { type: String },
	programa:{ type: [Programa] },
	adicional:{ type: [Adicional] },
	docacta: { type: File },
	docrfc: { type: File },
	docdomfiscal: { type: File },
	doccluni: { type: File },
	docrepresentantelegal: { type: File },
	doccaratula: { type: File },
	docconstancia: { type: File },
	docreporteanual: { type: File },
	docacuse: { type: File },
	observaciones: { type: String },
	fechavalidacion: { type: Fecha }
});

module.exports =  mongoose.model('organizaciones', Organizacion); 