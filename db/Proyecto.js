const mongoose = require("mongoose");

var Fecha = Number;
var File = String;

var Concepto = new mongoose.Schema({
	producto: { type: String },
	cantidad: { type: Number },
	caracteristica: { type: String },
	costo_total: { type: Number },
	archivo:{ type: File }
});


var Actividades = new mongoose.Schema({
	actividad1: { type: Boolean },
	actividad2: { type: Boolean },
	actividad3: { type: Boolean },
	actividad4: { type: Boolean },
	actividad5: { type: Boolean },
	actividad6: { type: Boolean },
	actividad7: { type: Boolean },
	actividad8: { type: Boolean },
	actividad9: { type: Boolean },
	actividad10: { type: Boolean },
	actividad11: { type: Boolean },
	actividad12: { type: Boolean },
	actividad13: { type: Boolean },
	actividad14: { type: Boolean },
	actividad15: { type: Boolean },
	actividad16: { type: Boolean },
	actividad17: { type: Boolean },
	actividad18: { type: Boolean }
});

var Objetivos = new mongoose.Schema({
	objetivo1: { type: Boolean },
	objetivo2: { type: Boolean },
	objetivo3: { type: Boolean },
	objetivo4: { type: Boolean },
	objetivo5: { type: Boolean },
	objetivo6: { type: Boolean },
	objetivo7: { type: Boolean },
	objetivo8: { type: Boolean },
	objetivo9: { type: Boolean },
	objetivo10: { type: Boolean },
	objetivo11: { type: Boolean },
	objetivo12: { type: Boolean },
	objetivo13: { type: Boolean },
	objetivo14: { type: Boolean },
	objetivo15: { type: Boolean },
	objetivo16: { type: Boolean },
	objetivo17: { type: Boolean },
	objetivo18: { type: Boolean }
});


var Proyecto = new mongoose.Schema({
	nombre: { type: String },
	beneficiario: { type: String },
	rango:{ type: String },
	municipio: { type: String },
	comunidades: { type:[String] },
	descripcion: { type: String },
	actividades: { type: [Boolean] },
	objetivos: { type: [Boolean] },
	conceptos: { type: [Concepto] },
	fecharegistro: {  type: Fecha}
});

module.exports =  mongoose.model('proyectos', Proyecto); 