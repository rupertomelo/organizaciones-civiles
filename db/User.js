var mongoose = require('mongoose');

var Usuarios = new mongoose.Schema({
	rfcorg: String,
	nombreorg: String,
	correoorg: String,
	passorg: String,
	fecha_registro: String
});

module.exports = mongoose.model('users', Usuarios);
