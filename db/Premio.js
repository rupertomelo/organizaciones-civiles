const mongoose = require("mongoose");

var Fecha = Number;


var Premio = new mongoose.Schema({
	nombreorganizacion: { type: String },
	nocluniorganizacion: { type: String },
	rfcorganizacion: { type: String },
	correoorganizacion: { type: String },
	donatariaorganizacion: { type: String },
	constitucionorganizacion: { type: Fecha },
	telefonoorganizacion: { type: String },
	paginaorganizacion: { type: String },
	facebookorganizacion: { type: String },
	otraredorganizacion: { type: String },
	nopersonasorganizacion: { type: Number },
	voluntariosorganizacion: { type: Number },
	misionorganizacion: { type: String },
	visionorganizacion: { type: String },
	juntaorganizacion: { type: String },
	objetivoorganizacion: { type: String },
	fecharegistro: { type: Fecha }
});

module.exports =  mongoose.model('premio', Premio);