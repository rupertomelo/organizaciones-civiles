const mongoose = require('mongoose');
const jwt = require('jwt-simple');
const SecretKey = "ReehzerModel 18";

const Location = new mongoose.Schema({
    ip: String,
    lat: { type: Number, required: false },
    long: { type: Number, required: false },
    interest: { type: Number, required: false }
});

const Reehzer = new mongoose.Schema({
    imei: String,
    locations: [Location]
});

const ReehzerModel = mongoose.model('reehzerdata', Reehzer);

const ProcessSolicitude = (req, res, next) => {
    const IMEI = req.params.imei;
    switch(IMEI){
        case "request":
                const myImei = jwt.encode({ date: Number(new Date()) }, SecretKey);
                const myFirstLocation = { ip: (req.headers['x-forwarded-for'] || req.connection.remoteAddress) };
                if(req.body && req.body.lat && req.body.long){
                    myFirstLocation["lat"] = req.body.lat;
                    myFirstLocation["long"] = req.body.long;
                    if(req.body.interest){
                        myFirstLocation["interest"] = req.body.interest;
                    }
                }
                const newData = new ReehzerModel({ imei: myImeim, locations: [myFirstLocation] });
                newData.save((err, result) => {
                    if(!err){
                        return res.send({ myKey: myImei });
                    }else{
                        return res.send({ error: null });
                    }
                });

            break;
        default:
                ReehzerModel.findOne({ imei: IMEI }).exec().then((result) => {
                    const mytLocation = { ip: (req.headers['x-forwarded-for'] || req.connection.remoteAddress) };
                    if(req.body && req.body.lat && req.body.long){
                        mytLocation["lat"] = req.body.lat;
                        mytLocation["long"] = req.body.long;
                        if(req.body.interest){
                            mytLocation["interest"] = req.body.interest;
                        }
                    }
                    result.locations.push(mytLocation);
                    result.save((err) => {
                        if(!err){
                            return res.send({ next: Number(new Date()) + 60000 });
                        }else{
                            return res.send({ error: null });
                        }
                    });
                }).catch(() => {
                    return res.send({ next: Number(new Date()) + 60000 });
                });
            break;
    }
};


module.exports = (app) => {
    app.post('/analytics/:imei', ProcessSolicitude);
};