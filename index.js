"use strict";

const port = 8075;
const express = require('express');
const app = express();

require("./middleware")(app);
require("./router")(app);
require("./private")(app);

app.listen(port, () => {
  console.log('Organizaciones civiles running on port ', port, '!');
});