const jwt = require('jwt-simple');
const OrganizacionDB = require("../db/Organizacion");
const ProyectoDB = require("../db/Proyecto");
const PremioDB = require("../db/Premio");
const EquipamientoDB = require("../db/Equipamiento");
const UserDB = require("../db/User");
const secret = "0r64n1Z4c1oN3s";

const MiddlewareUserProd = (request, response, next) => {
  try{
    const userFind = jwt.decode(request.cookies.auth, secret);
    UserDB.findOne({ rfcorg: userFind.rfcorg }, (err, user) => {
      if(!err && user){
        response.user = JSON.parse(JSON.stringify(user));
        next();
      }else{
        return response.redirect("/Login");
      }
    });
  }catch(e){
    return response.redirect("/Login");
  }
};

const MiddlewareRegistros = (req, res, next) => {
  OrganizacionDB.find({estatus:"Registro"}, (err, reg) => {
    res.reg = JSON.parse(JSON.stringify(reg));
    next();
    // console.log(res.reg);
  });
};

const MiddlewareEstatusValidadas = (req, res, next) => {
  OrganizacionDB.count({estatus:"Validado"}, (err, reg) => {
    res.regvalidado = JSON.parse(JSON.stringify(reg));
    next();
    // console.log(res.reg);
  });
};

const MiddlewareEstatusRechazadas = (req, res, next) => {
  OrganizacionDB.count({estatus:"Rechazado"}, (err, reg) => {
    res.regrechazadas = JSON.parse(JSON.stringify(reg));
    next();
    // console.log(res.reg);
  });
};

const MiddlewareTotales = (req, res, next) => {
  OrganizacionDB.count({}, (err, reg) => {
    res.regtotales = JSON.parse(JSON.stringify(reg));
    next();
    // console.log(res.reg);
  });
};

const MiddlewareRegistrosPremios = (req, res, next) => {
  PremioDB.find({}, (err, reg) => {
    res.regpremios = JSON.parse(JSON.stringify(reg));
    next();
    // console.log(res.reg);
  });
};

const MiddlewareRegistrosProyectos = (req, res, next) => {
  ProyectoDB.find({}, (err, reg) => {
    res.regproy = JSON.parse(JSON.stringify(reg));
    next();
    // console.log(res.reg);
  });
};

const MiddlewareRegistrosEquipamiento = (req, res, next) => {
  EquipamientoDB.find({}, (err, reg) => {
    res.regequipamiento = JSON.parse(JSON.stringify(reg));
    next();
    // console.log(res.reg);
  });
};

const Filtros = (req, res, next) =>{
	console.log(req.query);

	const query = {};

  if(req.query.text){
    query["$text"] = {
      "$search": req.query.text
    };
  }else{
  	query["$text"] = {
      "$search": ""
    };
  }

  res.aggregateFiltro = [];

  res.aggregateFiltro = res.aggregateFiltro.concat([
    { $match: query }
  ]);

  OrganizacionDB.aggregate(res.aggregateFiltro, (err,org)=>{
    res.org = JSON.parse(JSON.stringify(org));
    next();
  });
};

const MiddlewareConRegistro = (req, res, next) => {
	var user = res.user;
	var nombreorg = user.nombreorg;
 	OrganizacionDB.count({ nombre: nombreorg }, (err, organizacion) => {
    	res.organizacion = JSON.parse(JSON.stringify(organizacion));
   		next();
  })
};

const MiddlewareRegistro = (req, res, next) => {
	var user = res.user;
	var nombreorg = user.nombreorg;
 	OrganizacionDB.find({ nombre: nombreorg }, (err, organizacion) => {
    	res.registro = JSON.parse(JSON.stringify(organizacion));
   		next();
  })
};

const MiddlewareUser = MiddlewareUserProd;

module.exports = (app) => {
	app.set('view engine', 'ejs');

	app.get("/", (req, res, next) => {
		return res.render("contents/Inicio", {
			data: {
				title: "Organizaciones Civiles",
			}
		});
	});

	app.get("/Inicio", Filtros, (req, res, next) => {
		var org = res.org.length;
		if(org > 0){
			return res.render("contents/View", { data: { title: "Sociedades Civiles", org:res.org } });
		}else{
			return res.redirect("/");
		}
	});

	app.get("/Alta", (req, res, next) => {
		return res.render("contents/Alta", {
			data: {
				title: "Nueva organizaciones"
			}
		});
	});

	app.get("/Respuesta",(req, res, next) => {
		return res.render("contents/Respuesta", {
				data: {
					title: "Respuesta" ,
					name: "platform"
				}
		});
	});

	app.get("/Respuesta2",(req, res, next) => {
		return res.render("contents/respuesta2", {
			data: {
				title: "Respuesta" ,
				name: "platform" }
			});
	});

	app.get("/Registro", MiddlewareUser,  (req, res, next)=>{
		return res.render("contents/RegistroOrg", {
			data: {
				title: "Registro",
				user: res.user,
				org:undefined
			}
		});
	});

	app.get("/Login", (req, res, next)=>{
			return res.render("contents/Login",{
				data: {title:"Login"}
			});
	});

	app.get("/Logout", MiddlewareUser, (request, response) => {
		try{
			response.cookie('auth', '', { maxAge: 0, httpOnly: true });
			return response.redirect("/");
		}catch(e){
			return response.redirect("/");
		}
	});

	app.get("/Perfil", (req, res, next)=>{
		return res.render("contents/perfil",{data: {title:"Perfil" , name:"platform"} });
	});

	app.get("/Proyecto", MiddlewareUser, (req, res, next)=>{
		return res.render("contents/proyecto",{
			data: {
				title:"Proyecto",
				user: res.user
			}
		});
	});

	app.get("/Equipamiento", MiddlewareUser, (req, res, next)=>{
		return res.render("contents/Equipamiento",{
			data:{
				title:"Equipamiento",
				user: res.user
			}
		});
	});

	app.get("/Premio", MiddlewareUser,(req, res, next)=>{
		console.log(res.organizacion);
		return res.render("contents/Premio",{
			data: {
				title:"Premio",
				user:res.user
			}
		});
	});

	app.get("/Gracias", (req, res, next)=>{
		return res.render("contents/Gracias",{
			data: {
				title:"Gracias",
				name:"platform"
			}
		});
	});

	app.get("/Acciones", MiddlewareUser, (req, res, next)=>{
		return res.render("contents/Acciones",{
			data:{
				title:"Acciones",
				user: res.user
			}
		});
	});

	app.get("/validar", MiddlewareUser, MiddlewareRegistros,(req, res, next) => {
      return res.render("contents/Validar", {
        data: {
          title: "Validar",
          user: res.user,
          registros: res.reg,
        },
      });
    }
  );

  app.get("/validar/:_id", MiddlewareUser, (req, res, next) => {
    OrganizacionDB.findOne({_id:req.params._id}, ( err, org) => {
      res.render("contents/RegistroOrg",{
        data:{
          title: "Validar",
          user: res.user,
          org: JSON.parse(JSON.stringify(org))
        }
      });
    });
  });

  app.get("/update/organizacion/:_id", MiddlewareUser, (req, res, next) => {
    OrganizacionDB.findOne({_id:req.params._id}, ( err, org) => {
      res.render("contents/RegistroOrg",{
        data:{
          title: "Actualiza tu información",
          user: res.user,
          org: JSON.parse(JSON.stringify(org))
        }
      });
    });
  });

  app.get("/ver/premio/:_id", MiddlewareUser, (req, res, next) => {
    PremioDB.findOne({_id:req.params._id}, ( err, doc) => {
      res.render("contents/Premio",{
        data:{
          title: "Ver Premio",
          user: res.user,
          premio: JSON.parse(JSON.stringify(doc))
        }
      });
    });
  });

  app.get("/ver/proyecto/:_id", MiddlewareUser, (req, res, next) => {
    ProyectoDB.findOne({_id:req.params._id}, ( err, doc) => {
      res.render("contents/Proyecto",{
        data:{
          title: "Ver Proyecto",
          user: res.user,
          proy: JSON.parse(JSON.stringify(doc))
        }
      });
    });
  });

  app.get("/ver/equipamiento/:_id", MiddlewareUser, (req, res, next) => {
    EquipamientoDB.findOne({_id:req.params._id}, ( err, doc) => {
      res.render("contents/Equipamiento",{
        data:{
          title: "Ver Proyecto",
          user: res.user,
          eq: JSON.parse(JSON.stringify(doc))
        }
      });
    });
  });

  app.get("/Bienvenido", MiddlewareUser, MiddlewareConRegistro, MiddlewareRegistro,MiddlewareRegistros,MiddlewareEstatusValidadas, MiddlewareTotales, MiddlewareEstatusRechazadas, (req, res, next)=>{
		console.log(res.registro);
		return res.render("contents/bienvenido",{
			data: {
				title:"Bienvenido",
				user:res.user,
				organizacion: res.organizacion,
				registro: res.registro,
				registros:res.reg,
				validadas:res.regvalidado,
				rechazadas:res.regrechazadas,
				totales: res.regtotales
			}
		});
	});

  app.get("/Proyectos", MiddlewareUser, MiddlewareRegistrosProyectos, (req, res, next) => {
    return res.render("contents/admin/Proyectos-admin", {
        data: {
          title: "Proyectos",
          user: res.user,
          proyectos: res.regproy
        },
      });
  });

  app.get("/Equipamientos", MiddlewareUser, MiddlewareRegistrosEquipamiento, (req, res, next) => {
    return res.render("contents/admin/Equipamiento-admin", {
        data: {
          title: "Equipamientos",
          user: res.user,
          equipamiento: res.regequipamiento
        },
      });
  });

  app.get("/Premios", MiddlewareUser, MiddlewareRegistrosPremios, (req, res, next) => {
    return res.render("contents/admin/Premios-admin", {
        data: {
          title: "Premios",
          user: res.user,
          premios: res.regpremios,
        },
      });
  });

};