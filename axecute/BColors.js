"use strict";

const loadingSpinner = require('loading-spinner');

const Exit = () => {
  loadingSpinner.stop();
  process.exit(0);
};

const prompt = require('prompt');
prompt.start();

prompt.get(['Nombre', 'email'], function (err, result) {
	console.log('Command-line input received:');
	//console.log('  username: ' + result.username);

	loadingSpinner.start(100, {
	    clearChar: true
	});


	setTimeout(Exit,2000);
});