const session = require('express-session');
const connectMongo = require('connect-mongo');
const MongoStore = connectMongo(session);
const PathMongoose = 'mongodb://localhost:27017/sessionswww';

module.exports = (app) => {

    const options = {
        url: PathMongoose,
        collection: 'sessionswww'
    };

	app.use(session({
	    secret: "hsuiushc156543qwdjioashfqhfiOASHFIO",
	    store: new MongoStore(options),
        resave: false,
        saveUninitialized: true
	}));
};