const mongoose = require('mongoose');
const Grid = require('gridfs-stream');

module.exports = (app) => {
	mongoose.connect('mongodb://127.0.0.1:27017/sociedades');
	const conn = mongoose.connection;
	conn.once('open', function () {
		Grid.mongo = mongoose.mongo;
		app.grid_file_system = Grid(conn.db);
	});
};